package ru.icl.st.bpm.bpmplatform.rest.response;

public class StartingProcessResponse extends GenericResponse {

    String activeTaskId;

    String startTime;


    public String getActiveTaskId() {
        return activeTaskId;
    }

    public void setActiveTaskId(String activeTaskId) {
        this.activeTaskId = activeTaskId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
