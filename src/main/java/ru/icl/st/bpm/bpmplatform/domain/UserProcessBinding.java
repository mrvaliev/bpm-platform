package ru.icl.st.bpm.bpmplatform.domain;

import javax.persistence.*;

@Entity
@Table(name = "user_process_binding")
public class UserProcessBinding {

    @EmbeddedId
    UserProcessBindingId id;

    @ManyToOne
    @JoinColumn(name = "process_role")
    RoleInProcess roleInProcess;

    @Enumerated(value = EnumType.ORDINAL)
    ProcessType processType;

    @Enumerated(value = EnumType.ORDINAL)
    ProcessStatus status;

    public enum ProcessType {
        APPLICATION_APPROVING
    }

    public enum ProcessStatus {
        NEW,
        ACTIVE,
        COMPLETED,
        CANCELED
    }

    public UserProcessBinding(User user, String procId, RoleInProcess role, ProcessType type) {
        id = new UserProcessBindingId(user, procId);
        roleInProcess = role;
        status = ProcessStatus.ACTIVE;
        processType = type;
    }

    public UserProcessBinding() {
    }

    public UserProcessBindingId getId() {
        return id;
    }

    public RoleInProcess getRoleInProcess() {
        return roleInProcess;
    }

    public ProcessType getProcessType() {
        return processType;
    }

    public ProcessStatus getStatus() {
        return status;
    }
}
