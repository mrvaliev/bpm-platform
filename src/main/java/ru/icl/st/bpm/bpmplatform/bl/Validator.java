package ru.icl.st.bpm.bpmplatform.bl;

import org.activiti.engine.delegate.DelegateExecution;

public interface Validator {

    boolean validate(DelegateExecution execution);
}
