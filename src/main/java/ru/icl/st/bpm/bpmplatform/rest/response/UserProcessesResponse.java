package ru.icl.st.bpm.bpmplatform.rest.response;

public class UserProcessesResponse extends GenericResponse {

    String currentProcId;

    String assignedTask;

    public String getCurrentProcId() {
        return currentProcId;
    }

    public void setCurrentProcId(String currentProcId) {
        this.currentProcId = currentProcId;
    }
}
