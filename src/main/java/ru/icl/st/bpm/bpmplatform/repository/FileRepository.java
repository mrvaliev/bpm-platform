package ru.icl.st.bpm.bpmplatform.repository;

import ru.icl.st.bpm.bpmplatform.domain.AppFile;

import java.io.FileInputStream;
import java.io.InputStream;

public interface FileRepository {

    AppFile store(String fileName, InputStream fileContent);

    public AppFile getFileMetadata(Long fileId);

    byte[] readFile(String fileUri);

}
