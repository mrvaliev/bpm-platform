package ru.icl.st.bpm.bpmplatform.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.icl.st.bpm.bpmplatform.ApplicationValidationProcessService;
import ru.icl.st.bpm.bpmplatform.rest.response.ApproveAppResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.ProcessStatusResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.SendApplicationResponse;

import java.io.IOException;

@RestController
@RequestMapping("/process/application-validation")
public class ApplicationValidationProcessController {

    @Autowired
    ApplicationValidationProcessService processService;

    @RequestMapping(value = "start", method = RequestMethod.POST)
    public Object start() {
        Object user = getCurrentUser();
        return processService.startProcess(user, null);
    }


    @RequestMapping(value = "{procId:.+}/send-app", method = RequestMethod.POST)
    public SendApplicationResponse sendApplication(@PathVariable("procId") String procId,
                                                   @RequestParam("docName") String docName,
                                                   @RequestParam("approverName") String approverName,
                                                   @RequestParam("file") MultipartFile file) throws IOException {
        Object user = getCurrentUser();
        return processService.sendNewApplication(procId, file, docName, approverName, user);
    }

    @RequestMapping(value = "{procId:.+}/approve", method = RequestMethod.POST)
    public ApproveAppResponse approveApplication(@PathVariable("procId") String procId,
                                                 @RequestParam("approve") Boolean approve) {
        return processService.approveApplication(procId, approve, getCurrentUser());
    }

    @RequestMapping(value = "{procId:.+}/status", method = RequestMethod.GET)
    public ProcessStatusResponse getProcessStatus(@PathVariable("procId") String procId) {
        return processService.getProcessStatus(procId);
    }

    @RequestMapping(value = "{procId:.+}/get-document", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getDocumentForProcess(@PathVariable("procId") String procId,
                                        @RequestParam(value = "fileId", required = false) Long fileId) {
        return processService.getFileOfProcess(procId, fileId);
    }

    @RequestMapping(value = "/by-user", method = RequestMethod.GET)
    public ProcessStatusResponse getProcessByUser(@RequestParam("username") String userName) {
        Object user = getCurrentUser();
        return processService.getActiveProcessesForUser(user);
    }

    @RequestMapping(value = "/by-current-user", method = RequestMethod.GET)
    public ProcessStatusResponse getProcessByUser() {
        Object user = getCurrentUser();
        return processService.getActiveProcessesForUser(user);
    }

    private Object getCurrentUser() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user;
    }


}
