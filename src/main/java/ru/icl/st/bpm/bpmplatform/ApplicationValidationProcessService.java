package ru.icl.st.bpm.bpmplatform;


import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.icl.st.bpm.bpmplatform.bl.service.UserService;
import ru.icl.st.bpm.bpmplatform.domain.AppFile;
import ru.icl.st.bpm.bpmplatform.domain.Application;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.domain.UserProcessBinding;
import ru.icl.st.bpm.bpmplatform.repository.ApplicationRepository;
import ru.icl.st.bpm.bpmplatform.repository.FileRepository;
import ru.icl.st.bpm.bpmplatform.repository.UserProcessBindingRepository;
import ru.icl.st.bpm.bpmplatform.repository.UserRepository;
import ru.icl.st.bpm.bpmplatform.rest.exception.IllegalRightException;
import ru.icl.st.bpm.bpmplatform.rest.response.ApproveAppResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.ProcessStatusResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.SendApplicationResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.StartingProcessResponse;
import sun.misc.IOUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ApplicationValidationProcessService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private DateFormat dateFormat = DateFormat.getDateInstance();

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;


    @Autowired
    private IdentityService identityService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserProcessBindingRepository userProcessBindingRepository;

    @Autowired
    @Qualifier("diskFileRepository")
    private FileRepository fileRepository;

    Map<String, ProcessInstance> activeProcesses = new HashMap();

    /**
     * init process
     *
     * @param user        initator of process;
     * @param applicantId who is responsible for send application
     * @return
     */
    public StartingProcessResponse startProcess(Object user, String applicantId) {
        StartingProcessResponse response = new StartingProcessResponse();
        // get user-initiator
        User initiator;
        if (user instanceof User) {
            initiator = (User) user;
        } else {
            UserDetails userDetails = (UserDetails) (user);
            initiator = userRepository.findOne(userDetails.getUsername());
        }

        Map<String, Object> variables = new HashMap<>();
        // another applicant may be assigned
        if (applicantId != null) {
            variables.put("applicant", applicantId);
        } else {
            variables.put("applicant", initiator.getUsername());
        }

        ProcessInstance process = runtimeService.startProcessInstanceByKey(
                "APPLICATION_APPROVING", variables);


        UserProcessBinding binding = new UserProcessBinding(initiator, process.getId(), null,
                UserProcessBinding.ProcessType.APPLICATION_APPROVING);
        userProcessBindingRepository.save(binding);
        activeProcesses.put(initiator.getUsername(), process);
        response.setMsg("OK");
        response.setProcId(process.getId());
        response.setActiveTaskId(process.getActivityId());
        response.setStartTime(dateFormat.format(process.getStartTime()));
        return response;
    }

    /**
     * perform sending application
     *
     * @param file     document file for
     * @param docName  name of document for process logic
     * @param approver user id
     * @param user     the current activity performer
     * @throws IOException
     */
    public SendApplicationResponse sendNewApplication(String procId, MultipartFile file, String docName, String approver, Object user) throws IOException {
        SendApplicationResponse response = new SendApplicationResponse();
        response.setProcId(procId);

        UserDetails userDetails = (UserDetails) user;
        String userName = userDetails.getUsername();
        boolean hasPermission = hasRole(userDetails.getAuthorities(), "user");
        if (!hasPermission)
            throw new IllegalRightException("User " + userName + "cannot send application");

        Task task = findTasksByProcessAndUser(userName, procId);

        AppFile appFile = fileRepository.store(file.getOriginalFilename(), file.getInputStream());
        Map taskData = new HashMap();
        Application application = new Application();
        application.setFileName(file.getName());
        application.setApplicantName(userDetails.getUsername());
        application.setApproverName(approver);
        application.getFiles().add(appFile);
        applicationRepository.save(application);
        taskData.put("application", application);
        taskData.put("approver", approver);
        taskData.put("applicationId", application.getId());
        taskService.complete(task.getId(), taskData);
        Task nextTask = getCurrentTask(procId);
        nextTask.setAssignee(userDetails.getUsername());
        User userApprover = userRepository.findOne(approver);
        UserProcessBinding processBinding = new UserProcessBinding(userApprover, procId, null,
                UserProcessBinding.ProcessType.APPLICATION_APPROVING);

        userProcessBindingRepository.save(processBinding);
        response.setFileId(appFile.getId());
        return response;
    }

    public ApproveAppResponse approveApplication(String procId, boolean approve, Object user) {

        User userApprover;
        ApproveAppResponse response = new ApproveAppResponse();

        if (user instanceof User) {
            userApprover = (User) user;
        } else {
            String username = ((UserDetails) user).getUsername();
            userApprover = userRepository.findOne(username);
        }

        Task task = findTasksByProcessAndUser(userApprover.getUsername(), procId);
        Map vars = runtimeService.getVariables(procId);
        Long id = (Long) vars.get("applicationId");
        Application app = applicationRepository.findOne(id);
        vars.put("applicationApproved", approve);
        taskService.complete(task.getId(), vars);
        response.setMsg("OK");
        return response;
    }

    public ProcessStatusResponse getProcessStatus(String procId) {
        return getProcessStatusResponse(procId);
    }

    public byte[] getFileOfProcess(String procId, Long fileId) {
        Map<String, Object> vars = runtimeService.getVariables(procId);
        Long id = (Long) vars.get("applicationId");
        Application app = applicationRepository.findOne(id);
        List<AppFile> files = app.getFiles();
        for (AppFile appFile : files) {
            if (appFile.getId().equals(fileId) || fileId == null) {
                byte[] content = fileRepository.readFile(appFile.getURI());
                return content;
            }
        }
        throw new IllegalStateException("Process has no file with id=" + fileId);
    }

    public ProcessStatusResponse getActiveProcessesForUser(Object user) {
        ProcessStatusResponse response = new ProcessStatusResponse();
        List<UserProcessBinding> list = userProcessBindingRepository.
                findByIdUserAndProcessTypeAndStatus((User) user,
                        UserProcessBinding.ProcessType.APPLICATION_APPROVING,
                        UserProcessBinding.ProcessStatus.ACTIVE);

        if (!list.isEmpty()) {
            String procId = list.get(0).getId().getProcId();
            return getProcessStatusResponse(procId);
        }
        return response;
    }

    public Task findTasksByProcessAndUser(String userId, String procId) {
        List<Task> tasks = taskService
                .createTaskQuery().processInstanceId(procId).taskAssignee(userId).list();
        if (!tasks.isEmpty())
            return tasks.get(0);

        String msg = String.format("User % has no assigned task for process %s", userId, procId);
        throw new RuntimeException(msg);
    }


    private Task getCurrentTask(String procId) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procId).list();
        if (!tasks.isEmpty())
            return tasks.get(0);
        throw new IllegalStateException("There is no current task for procId=" + procId);
    }


    private boolean hasRole(Collection<? extends GrantedAuthority> authorities, String role) {
        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().contains(role))
                return true;
        }
        return false;
    }

    private ProcessStatusResponse getProcessStatusResponse(String procId) {
        ProcessStatusResponse response = new ProcessStatusResponse();
        List<ProcessInstance> instances =
                runtimeService.createProcessInstanceQuery().processInstanceId(procId).list();

        if (instances.isEmpty()) {
            response.setHasError(true);
            response.setMsg("There is no process with id=" + procId);
        } else {
            ProcessInstance instance = instances.get(0);
            response.setProcId(instance.getId());
            response.setActivityId(instance.getActivityId());
            response.setEnded(instance.isEnded());
            response.setTaskName(getCurrentTask(procId).getName());
            response.setAssignedUser(getCurrentTask(procId).getAssignee());
            response.setStartTime(instance.getStartTime());
        }
        return response;
    }
}
