package ru.icl.st.bpm.bpmplatform.bl;


import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("validationChain")
public class ValidationChain implements JavaDelegate {

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    List<Validator> validators;

    Expression validatorBeanNames;

    @Autowired
    ApplicationContext applicationContext;

    @Override
    public void execute(DelegateExecution execution) {
        if (validatorBeanNames != null) {
            String beanNames = (String) validatorBeanNames.getValue(execution);
            validators = getValidators(beanNames);
        }
        int i = 0;
        boolean isValid = true;
        while (i < validators.size() && isValid) {
            isValid = validators.get(i).validate(execution);
            i++;
        }
        execution.setVariable("applicationValid", isValid);
    }


    private List<Validator> getValidators(String beanNames) {
        String[] beans = beanNames.trim().split(",");
        List<Validator> validators = new ArrayList<>();
        for (int i = 0; i < beans.length; i++) {
            Validator validator = (Validator) applicationContext.getBean(beans[i]);
            if (validator == null) {
                LOG.error("No validator bean found with name={}", beans[i]);
            } else {
                validators.add(validator);
            }
        }
        return validators;
    }
}
