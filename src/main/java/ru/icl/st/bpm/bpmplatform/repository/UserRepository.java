package ru.icl.st.bpm.bpmplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.icl.st.bpm.bpmplatform.domain.User;

public interface UserRepository extends JpaRepository<User,String> {
}
