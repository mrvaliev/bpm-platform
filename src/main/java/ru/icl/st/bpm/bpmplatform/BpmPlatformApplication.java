package ru.icl.st.bpm.bpmplatform;

import org.activiti.engine.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {BpmPlatformApplication.class})
public class BpmPlatformApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(BpmPlatformApplication.class, args);
    }


    //    @Bean
    public CommandLineRunner deployProcess(@Autowired RepositoryService service) {
        return strings -> {
            service.createDeployment()
                    .addClasspathResource("kuku/Developer_Hiring.bpmn20.xml")
                    .name("from-custom-dir")
                    .deploy();
        };
    }
}
