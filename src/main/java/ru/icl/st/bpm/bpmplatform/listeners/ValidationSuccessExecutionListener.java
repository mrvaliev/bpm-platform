package ru.icl.st.bpm.bpmplatform.listeners;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;


@Service("validationSuccessExecutionListener")
public class ValidationSuccessExecutionListener implements ExecutionListener {


    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationContext applicationContext;

    @Override
    public void notify(DelegateExecution execution) {
        LOG.info("Валидация заявления успешна");
    }
}
