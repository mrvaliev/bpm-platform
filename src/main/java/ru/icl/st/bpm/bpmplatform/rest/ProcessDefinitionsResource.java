package ru.icl.st.bpm.bpmplatform.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.icl.st.bpm.bpmplatform.bl.service.ProcessDefinitionService;

import java.io.IOException;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/process-definitions")
public class ProcessDefinitionsResource {


    @Autowired
    ProcessDefinitionService processDefinitionService;

    @RequestMapping(method = POST)
    public ResponseEntity createProcessDefinition(@RequestParam("file") MultipartFile file) throws IOException {
        Map info = processDefinitionService.deployProcessDefinition(file.getOriginalFilename(), file.getInputStream());
        return new ResponseEntity(info, HttpStatus.OK);
    }

    @RequestMapping(value = "{procDefId}/state", method = RequestMethod.GET)
    public ResponseEntity getProcessDefinitionSuspensionState(@PathVariable("procDefId") String procDefId) {
        Map info = processDefinitionService.getProcessDefinitionSuspensionState(procDefId);
        return new ResponseEntity(info, HttpStatus.OK);
    }

    @RequestMapping(value = "{procDefId}/suspend", method = POST)
    public void suspendProcessDefinition(@PathVariable("procDefId") String procDefId) {
        processDefinitionService.suspendProcessDefinition(procDefId);
    }


    @RequestMapping(value = "{procDefId}/activate", method = POST)
    public void activateProcessDefinition(@PathVariable("procDefId") String procDefId) {
        processDefinitionService.activateProcessDefinition(procDefId);
    }

//    @RequestMapping(value = "{deploymentId}", method = DELETE)
//    public void deleteDeployment(@PathVariable("deploymentId") String deploymentId) {
//        processDefinitionService.deleteDeployment(deploymentId);
//    }

    @RequestMapping(value = "{procDefId}", method = DELETE)
    public void deleteDeployment(@PathVariable("procDefId") String procDefId) {
        processDefinitionService.deleteProcessDefinition(procDefId);
    }

}
