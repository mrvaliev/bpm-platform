package ru.icl.st.bpm.bpmplatform.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {IllegalRightException.class})
    ResponseEntity handleIllegalRightExection(RuntimeException ex) {
        return new ResponseEntity<>("Permission denied", HttpStatus.OK);
    }

}
