package ru.icl.st.bpm.bpmplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.icl.st.bpm.bpmplatform.domain.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
