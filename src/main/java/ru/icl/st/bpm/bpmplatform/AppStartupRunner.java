package ru.icl.st.bpm.bpmplatform;


import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.icl.st.bpm.bpmplatform.bl.service.UserService;
import ru.icl.st.bpm.bpmplatform.domain.Application;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.repository.ApplicationRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Profile("!test")
//@Component
public class AppStartupRunner implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger("Cmd:");

    @Autowired
    ApplicationContext context;

    @Autowired
    UserService userService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    UserApplicationHandler userApplicationHandler;

    @Autowired
    ApplicationRepository applicationRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {


        userService.createDefaultUsers();
        Scanner scanner = new Scanner(System.in);
        String line;
        logger.info("Привет! Введите имя пользователя:");

        line = scanner.nextLine();
        User user = userService.findUserbyLogin(line);

        Map variables = new HashMap();
        variables.put("applicant", user);
        logger.info("Вы вошли как {}", user.getFirstName());

        // начало работы с процессом
        ProcessInstance process = runtimeService.startProcessInstanceByKey("APPLICATION_APPROVING", variables);

        userApplicationHandler.handle(taskService, variables, process.getId());

        List<Task> tasks = taskService.createTaskQuery().processInstanceId(process.getId()).list();

        while (!process.isEnded() && !tasks.isEmpty()) {
            tasks = taskService.createTaskQuery().processInstanceId(process.getId()).list();
            if (tasks.isEmpty()) break;
            Task task = tasks.get(0);
            String taskName = task.getTaskDefinitionKey();

            if (taskName.equalsIgnoreCase("fillApp")) {
                logger.info("Необходим заново отправить заявление:");
                userApplicationHandler.handle(taskService, variables, process.getId());

            } else if (taskName.equalsIgnoreCase("approving")) {
                logger.info("Необходимо соглсование заявление: ");
                logger.info("Параметры заявления: Файл: {}, Отправитель: {}");
                logger.info("Введите yes/no чтобы согласовать:");
                line = "";
                while (!(line.contains("yes") || line.contains("no"))) {
                    line = scanner.nextLine();
                    if (line.equals("yes")) {
                        variables.put("applicationApproved", true);
                    } else {
                        variables.put("applicationApproved", false);
                    }
                }
                taskService.complete(task.getId(), variables);
            }
        }

        SpringApplication.exit(context, (ExitCodeGenerator) () -> 0);
    }

    @Component
    public class UserApplicationHandler {

        private void handle(TaskService taskService, Map vars, String procID) {
            Scanner scanner = new Scanner(System.in);
            String line;

            String taskId = taskService.createTaskQuery().processInstanceId(procID).list().get(0).getId();

            logger.info("Введите название файла заявления");
            line = scanner.nextLine();

            Application application = new Application();
            application.setFileName(line);
            application.setApplicantName("user");
            application.setApproverName("manager");

            applicationRepository.save(application);

            vars.put("application", application);
            taskService.complete(taskId, vars);
        }

    }
}