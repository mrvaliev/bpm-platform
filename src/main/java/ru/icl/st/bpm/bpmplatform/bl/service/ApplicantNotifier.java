package ru.icl.st.bpm.bpmplatform.bl.service;


import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.icl.st.bpm.bpmplatform.domain.Application;
import ru.icl.st.bpm.bpmplatform.domain.User;

@Component("applicantNotifier")
public class ApplicantNotifier implements JavaDelegate {

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(DelegateExecution execution) {
        LOG.info("applicantNotifier works!");

        boolean approved = (Boolean) execution.getVariable("applicationApproved");
        Application application = (Application) execution.getVariable("application");

        if (approved) {
            LOG.info("{}, You application has approved", application.getApplicantName());
        } else {
            LOG.info("{}, You application has rejected", application.getApplicantName());
        }
    }
}
