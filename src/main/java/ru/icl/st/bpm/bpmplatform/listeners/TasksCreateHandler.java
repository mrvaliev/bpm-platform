package ru.icl.st.bpm.bpmplatform.listeners;


import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("taskHandler")
public class TasksCreateHandler implements TaskListener {

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Override
    public void notify(DelegateTask delegateTask) {
        String task = delegateTask.getName();
        LOG.info("Task {} created", task);
    }
}
