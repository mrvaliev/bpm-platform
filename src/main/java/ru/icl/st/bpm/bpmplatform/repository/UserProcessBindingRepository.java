package ru.icl.st.bpm.bpmplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.domain.UserProcessBinding;
import ru.icl.st.bpm.bpmplatform.domain.UserProcessBindingId;

import java.util.List;

public interface UserProcessBindingRepository extends JpaRepository<UserProcessBinding, UserProcessBindingId> {

    List<UserProcessBinding> findByIdUserAndProcessType(User user, UserProcessBinding.ProcessType processType);

    List<UserProcessBinding> findByIdUserAndProcessTypeAndStatus(User user,
                                                                 UserProcessBinding.ProcessType processType,
                                                                 UserProcessBinding.ProcessStatus status);
}
