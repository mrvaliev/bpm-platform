package ru.icl.st.bpm.bpmplatform.listeners;


import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("validationFailedExecutionListener")
public class ValidationFailedExecutionListener implements ExecutionListener {

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Override
    public void notify(DelegateExecution execution) {
        LOG.info("Валидация заявления не пройдена");
    }
}
