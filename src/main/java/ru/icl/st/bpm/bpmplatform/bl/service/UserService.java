package ru.icl.st.bpm.bpmplatform.bl.service;


import org.activiti.engine.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.icl.st.bpm.bpmplatform.domain.Group;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.repository.GroupRepository;
import ru.icl.st.bpm.bpmplatform.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    IdentityService activitiUserService;

    private static String defpasswd = "123qwe";

    public User findUserbyLogin(String login) {
        User user = userRepository.findOne(login);
        return user;
    }


    @PostConstruct
    public void createDefaultUsers() {

        Group group = new Group("user", "Users");
        Group group2 = new Group("manager", "Managers");

        groupRepository.save(group);
        groupRepository.save(group2);


        User user1 = new User();
        user1.setUserName("user");
        user1.setFirstName("Айрат");
        user1.setEmail("user1@mail.ru");
        user1.getGroups().add(group);
        user1.setPassword(defpasswd);
        userRepository.save(user1);

        User user2 = new User();
        user2.setUserName("manager");
        user2.setFirstName("Jon");
        user2.setPassword(defpasswd);
        user2.setEmail("user2@mail.ru");
        user2.getGroups().add(group2);
        userRepository.save(user2);

    }

    public void createActivitUsers() {


        org.activiti.engine.identity.User actUser =
                activitiUserService.newUser("user");
        actUser.setPassword(defpasswd);

        org.activiti.engine.identity.User actUser2 =
                activitiUserService.newUser("manager");
        actUser2.setPassword(defpasswd);


        activitiUserService.saveUser(actUser);
        activitiUserService.saveUser(actUser2);
    }
}
