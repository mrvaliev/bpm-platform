package ru.icl.st.bpm.bpmplatform.domain;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "\"group\"")
public class Group implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String role;

    String name;

    @Override
    public String getAuthority() {
        return role;
    }

    public Group(){}

    public Group(String role, String name) {
        this.role = role;
        this.name = name;
    }
}
