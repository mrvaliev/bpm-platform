package ru.icl.st.bpm.bpmplatform.domain;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class UserProcessBindingId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;


    @Column(name = "process_instance")
    String procId;


    public UserProcessBindingId() {
    }

    public UserProcessBindingId(User user, String procId) {
        this.user = user;
        this.procId = procId;
    }

    public User getUser() {
        return user;
    }

    public String getProcId() {
        return procId;
    }
}
