package ru.icl.st.bpm.bpmplatform.bl.validators;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import ru.icl.st.bpm.bpmplatform.bl.Validator;
import ru.icl.st.bpm.bpmplatform.domain.AppFile;
import ru.icl.st.bpm.bpmplatform.domain.Application;

import java.util.Arrays;
import java.util.List;

@Component
public class FileValidator implements Validator {

    public static final List<String> CONSTANTS = Arrays.asList("doc", "docx", "pdf");


    @Override
    public boolean validate(DelegateExecution execution) {
        Application application = (Application) execution.getVariable("application");
        if (application.getFiles().isEmpty())
            return false;
        String fileName = application.getFiles().get(0).getName();
        int ind = fileName.lastIndexOf(".");
        String ext = fileName.substring(ind + 1);
        return CONSTANTS.contains(ext);
    }
}
