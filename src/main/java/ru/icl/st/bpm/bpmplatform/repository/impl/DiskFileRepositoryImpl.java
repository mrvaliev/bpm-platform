package ru.icl.st.bpm.bpmplatform.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.icl.st.bpm.bpmplatform.domain.AppFile;
import ru.icl.st.bpm.bpmplatform.repository.FileRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service("diskFileRepository")
class DiskFileRepositoryImpl implements FileRepository {

    final String FILE_DIR = "files";

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Override
    public AppFile store(String fileName, InputStream fileContent) {

        BufferedOutputStream outputStream = null;
        BufferedInputStream inputStream = null;
        try {

            UUID uuid = UUID.randomUUID();
            Path path = Paths.get("./" + uuid.toString());

            outputStream = new BufferedOutputStream(new FileOutputStream(path.toFile()));
            inputStream = new BufferedInputStream(fileContent);
            byte[] bytes = new byte[fileContent.available()];
            inputStream.read(bytes, 0, bytes.length);
            outputStream.write(bytes, 0, bytes.length);
            outputStream.flush();
            inputStream.close();
            outputStream.close();
            AppFile file = new AppFile();
            file.setName(fileName);
            file.setURI(path.toString());
            return file;

        } catch (FileNotFoundException e) {
            LOG.error("file saving failed");
            throw new RuntimeException(e);
        } catch (IOException ex) {
            LOG.error("file saving failed");
            throw new RuntimeException(ex);
        } finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public AppFile getFileMetadata(Long fileId) {
        EntityManager em = getEm();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        AppFile file = em.find(AppFile.class, fileId);
        transaction.commit();
        return file;
    }

    @Override
    public byte[] readFile(String fileUri) {
        Path path = Paths.get(fileUri);
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            LOG.error("File not found: " + fileUri);
            throw new RuntimeException(e);
        }
    }

    private EntityManager getEm() {
        return entityManagerFactory.createEntityManager();
    }


}
