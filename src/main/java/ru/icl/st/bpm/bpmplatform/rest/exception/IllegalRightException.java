package ru.icl.st.bpm.bpmplatform.rest.exception;

public class IllegalRightException extends RuntimeException {

    public IllegalRightException(String message) {
        super(message);
    }

    public IllegalRightException() {
    }
}
