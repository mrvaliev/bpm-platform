package ru.icl.st.bpm.bpmplatform.rest;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;

@RestController
@PermitAll
public class AuthController {


    @RequestMapping("/hello")
    public String hello(String name) {
        return "Hello " + name;
    }



}
