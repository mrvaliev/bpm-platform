package ru.icl.st.bpm.bpmplatform.bl.service;


import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProcessDefinitionService {


    @Autowired
    RepositoryService repositoryService;

    private String temp_dir;

    public Map deployProcessDefinition(String filename, InputStream resource) throws IOException {

        byte[] buffer = new byte[resource.available()];
        resource.read(buffer);
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(buffer);
        Deployment deployment = repositoryService.createDeployment().name("rest-api-deployment")
                .addInputStream(filename, arrayInputStream).deploy();
        Map<String, Object> map = new HashMap<>();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .deploymentId(deployment.getId()).singleResult();
        map.put("deploymentId", deployment.getId());
        map.put("procDefId", processDefinition.getId());
        map.put("version", processDefinition.getVersion());
        return map;
    }


    public Map getProcessDefinitionSuspensionState(String id) {
        boolean suspended = repositoryService.isProcessDefinitionSuspended(id);
        Map<String, Object> map = new HashMap<>();
        map.put("isSuspended", suspended);
        return map;
    }

    public void suspendProcessDefinition(String id) {
        repositoryService.suspendProcessDefinitionById(id);
    }

    public void activateProcessDefinition(String id) {
        repositoryService.activateProcessDefinitionById(id);
    }

    public void deleteDeployment(String deploymentId) {
        repositoryService.deleteDeployment(deploymentId);
    }

    public void deleteProcessDefinition(String procDefId) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(procDefId).singleResult();
        repositoryService.deleteDeployment(processDefinition.getDeploymentId());
    }

}
