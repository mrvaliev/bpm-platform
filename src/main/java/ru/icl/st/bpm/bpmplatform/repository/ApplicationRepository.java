package ru.icl.st.bpm.bpmplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.icl.st.bpm.bpmplatform.domain.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

}
