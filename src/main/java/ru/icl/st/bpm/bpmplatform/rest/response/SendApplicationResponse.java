package ru.icl.st.bpm.bpmplatform.rest.response;

public class SendApplicationResponse extends GenericResponse {

    Long fileId;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
}
