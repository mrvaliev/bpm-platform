package ru.icl.st.bpm.bpmplatform.bl.validators;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.icl.st.bpm.bpmplatform.bl.Validator;
import ru.icl.st.bpm.bpmplatform.domain.Application;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.repository.UserRepository;

@Component
public class FioValidator implements Validator {

    final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Override
    public boolean validate(DelegateExecution execution) {
        LOG.info("validation is executing");
        Application application = (Application) execution.getVariable("application");
        String applicantName = application.getApplicantName();
        String approverName = application.getApproverName();
        User applicant = userRepository.findOne(applicantName);
        User approver = userRepository.findOne(approverName);
        return (applicant != null && applicant != null);
    }

}
