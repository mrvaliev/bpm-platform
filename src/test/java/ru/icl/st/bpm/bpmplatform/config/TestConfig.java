package ru.icl.st.bpm.bpmplatform.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:test-context.xml")
public class TestConfig {


    private static String EMAIL_USER_ADDRESS = "user@mail.ru";
    private static String EMAIL_USER_ADDRESS2 = "user2@mail.ru";

//    @Bean
//    public GreenMail runGreenMail() {
//        GreenMail mailServer = new GreenMail(ServerSetupTest.SMTP);
//        GreenMailUser user = mailServer.setUser(EMAIL_USER_ADDRESS, "password");
//        mailServer.setUser(EMAIL_USER_ADDRESS2, "password2");
//        mailServer.start();
//        return mailServer;
//    }

}
