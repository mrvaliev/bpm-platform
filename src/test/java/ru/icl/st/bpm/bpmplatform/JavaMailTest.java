package ru.icl.st.bpm.bpmplatform;

import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.junit.After;
import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.simplejavamail.email.Email;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.config.ServerConfig;
import org.simplejavamail.mailer.config.TransportStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.MimeMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class JavaMailTest {

    private GreenMail mailServer;

    @Autowired
    private Mailer mailClient;

    private static String EMAIL_USER_ADDRESS = "user@mail.ru";
    private static String EMAIL_USER_ADDRESS2 = "user2@mail.ru";

    private int mailServerPort;

    @Before
    public void runServer() {
        mailServer = new GreenMail(ServerSetupTest.SMTP);
        GreenMailUser user= mailServer.setUser(EMAIL_USER_ADDRESS, "password");

        mailServer.setUser(EMAIL_USER_ADDRESS2, "password2");
        mailServer.start();
        mailServerPort = mailServer.getSmtp().getPort();
    }

    @After
    public void stopServer() {
        mailServer.stop();
    }

    @Test
    public void testSendEmail() {

        Email email = new Email();
        email.addNamedToRecipients("Mister", EMAIL_USER_ADDRESS2);
        email.setFromAddress("Me", EMAIL_USER_ADDRESS);
        email.setSubject("Subject");
        email.setText("Hello mail");
        email.setId("the id");
        mailClient.sendMail(email);

        MimeMessage[] messages = mailServer.getReceivedMessages();

        assert (messages.length == 1);
    }

}
