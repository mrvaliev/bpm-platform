package ru.icl.st.bpm.bpmplatform;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.icl.st.bpm.bpmplatform.bl.service.UserService;
import ru.icl.st.bpm.bpmplatform.domain.Application;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.repository.ApplicationRepository;
import ru.icl.st.bpm.bpmplatform.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BpmPlatformApplicationTests {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void contextLoads() {
    }


    @Test
    public void startProcess() throws InterruptedException {

        userService.createDefaultUsers();
        Map variables = new HashMap();
        Application application = new Application();
        application.setFileName("application.docx");
        application.setApplicantName("user");
        application.setApproverName("manager");

        User user = userRepository.findOne("user");

        variables.put("application", application);
        variables.put("applicant", user);

        applicationRepository.save(application);
        // запустить процесс
        ProcessInstance process = runtimeService.startProcessInstanceByKey("APPLICATION_APPROVING");
        String procId = process.getId();
        String curTask;

        List<Task> taskList = taskService.createTaskQuery().processInstanceId(procId).orderByTaskName().asc().list();

        curTask = taskList.get(0).getId();

        // выполнить userTask: заполнение заявления
        taskService.complete(curTask, variables);

        // уведомление о небходимости согласования, возможно TaskListener

        taskList = taskService.createTaskQuery().processInstanceId(procId).orderByTaskName().asc().list();

        curTask = getCurTask(procId).getId();
        // согласование выполняем с помощью пользователя
        variables = new HashMap();
        variables.put("applicationApproved", true);
        taskService.complete(curTask, variables);
        taskList = taskService.createTaskQuery().processInstanceId(procId).list();
    }

    private Task getCurTask(String procId) {
        List<Task> taskList = taskService.createTaskQuery().processInstanceId(procId).orderByTaskName().asc().list();
        return taskList.get(0);
    }

}
