package ru.icl.st.bpm.bpmplatform;


import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import ru.icl.st.bpm.bpmplatform.domain.User;
import ru.icl.st.bpm.bpmplatform.rest.response.SendApplicationResponse;
import ru.icl.st.bpm.bpmplatform.rest.response.StartingProcessResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebMvc
@ActiveProfiles("test")
public class ApplicationValidationServiceTest {

    @Autowired
    ApplicationValidationProcessService service;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    IdentityService identityService;

    @Autowired
    TaskService taskService;


    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "userDetailsFromDb")
    public void testDownloadFile() throws Exception {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        StartingProcessResponse response = service.startProcess(userDetails, userDetails.getUsername());
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        MultipartFile testFile = TestUtils.getTestFile("testfile.txt");
        byte[] testFileContent = testFile.getBytes();
        SendApplicationResponse response2 = service.
                sendNewApplication(response.getProcId(), testFile, "doc_test", "user", user);
        Long fileId = response2.getFileId();
        byte[] data = service.getFileOfProcess(response.getProcId(), fileId);

        Assert.assertArrayEquals(data, testFileContent);

    }




}