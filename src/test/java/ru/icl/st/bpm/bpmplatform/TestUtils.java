package ru.icl.st.bpm.bpmplatform;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class TestUtils {

    public static MockMultipartFile getTestFile(String name) throws Exception {
        InputStream inputStream = TestUtils.class.getClassLoader().getResourceAsStream(name);
        byte[] fileContent = new byte[inputStream.available()];
        inputStream.read(fileContent);
        MockMultipartFile file = new MockMultipartFile("file", name,
                "application/xml", fileContent);
        return file;
    }

    public static Map getResponseParams(MvcResult result) throws IOException {
        String content = result.getResponse().getContentAsString();
        return new ObjectMapper().readValue(content, HashMap.class);
    }
}
