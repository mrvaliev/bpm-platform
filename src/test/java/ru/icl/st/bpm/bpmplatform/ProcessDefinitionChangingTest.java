package ru.icl.st.bpm.bpmplatform;


import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.icl.st.bpm.bpmplatform.bl.service.ProcessDefinitionService;

import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ProcessDefinitionChangingTest {


    @Autowired
    ProcessDefinitionService processDefinitionService;

    @Autowired
    RepositoryService repositoryService;

    static String procDefKey = "APPLICATION_APPROVING";

    Logger logger = LoggerFactory.getLogger("ProcessDefinitionChangingTest");

    @Autowired
    MockMvc webMvc;

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "userDetailsFromDb")
    public void test() throws Exception {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(procDefKey).singleResult();
        int v1 = processDefinition.getVersion();

        logger.info("Process {} has version {}", processDefinition.getKey(), processDefinition.getVersion());

        MockMultipartFile requestFile = TestUtils.getTestFile("processes/new/Approving_Application.bpmn20.xml");

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.fileUpload("/process-definitions").file(requestFile);

        MvcResult result = webMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        Map map = TestUtils.getResponseParams(result);
        int v2 = (int) map.get("version");
        Assert.assertEquals(v1 + 1, v2);
    }

}

