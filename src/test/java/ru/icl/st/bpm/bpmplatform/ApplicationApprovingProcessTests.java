package ru.icl.st.bpm.bpmplatform;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * интеграционный тест который проверяет всю работу процесса
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ApplicationApprovingProcessTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    private String processUrlPrefix = "/process/application-validation/";

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "userDetailsFromDb")
    public void doProcess() throws Exception {
        oneProcessWork();
    }


    public void oneProcessWork() throws Exception {
        // start process
        MvcResult result = mockMvc.perform(post(processUrlPrefix + "start"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
        Map<String, Object> responseParams = getResponseParams(result);

        String procId = (String) responseParams.get("procId");
        Assert.assertNotNull(responseParams.get("procId"));

        // send application file
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.fileUpload(processUrlPrefix + procId + "/send-app")
                        .file(TestUtils.getTestFile("testfile.txt"))
                        .param("approverName", "user")
                        .param("docName", "Документ такой");

        result = mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        responseParams = getResponseParams(result);
        Assert.assertNotNull(responseParams.get("fileId"));

        // approve application
        result = mockMvc.perform(post(processUrlPrefix + procId + "/approve")
                .param("approve", "true")).andExpect(status().isOk()).andReturn();

        result = mockMvc.perform(get(processUrlPrefix + procId + "/status"))
                .andExpect(status().isOk()).andReturn();
        responseParams = getResponseParams(result);

        Object values = responseParams.get("ended");
    }

    private Map getResponseParams(MvcResult result) throws IOException {
        String content = result.getResponse().getContentAsString();
        return new ObjectMapper().readValue(content, HashMap.class);
    }


}
