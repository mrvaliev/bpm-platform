package ru.icl.st.bpm.bpmplatform;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.icl.st.bpm.bpmplatform.domain.AppFile;
import ru.icl.st.bpm.bpmplatform.repository.FileRepository;

import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class FileRepositoryTest {


    @Autowired
    @Qualifier("diskFileRepository")
    FileRepository fileRepository;


    @Test
    public void testDiskFileRepostitory() throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("testfile.txt");
        AppFile file = fileRepository.store("testfile.doc", inputStream);
        System.out.println("stored file id= " + file.getId());
        assert (file != null);

    }

    @Test
    public void checkCurrentDirectory() {
        String workingDir = System.getProperty("user.dir");
        System.out.println("Current working directory : " + workingDir);
    }
}
